# demo-boot
项目介绍：     
------  
demo-boot基于SpringBoot,目标是实现自动办公,现阶段整合springmvc + shiro + mybatis + JSP等常见框架，包含用户管理、部门管理、权限管理、日志管理等。

欢迎fork，star；

欢迎加群学习架构：   [点此加入QQ群：](https://jq.qq.com/?_wv=1027&k=5xwNtbw) **317945902**  <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=1eddc0d8628b53f65129bb1e44fe342a1aaaa6dc7679739cfbefd7a2e86a15b5"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="SpringBoot SpringCloud Docker " title="SpringBoot SpringCloud Docker "></a>


默认管理员账号：admin  密码：admin
------
功能介绍：
------       
1. 用户管理 

2. 部门管理模块 

3. 权限管理

   ...

项目特点
------
1.项目基于SpringBoot，简化了大量的配置和Maven依赖
2.日志记录系统，记录用户的登陆、登出，用户执行的操作

所用框架
------
### 前端

 1. Bootstrap

 2. jQuery

 3. jqGrid

 4. jstree

 5. SweetAlert
    
    ...

### 后端

 1. SpringBoot
 2. MyBatis
 3. Spring
 4. JSP

    ...

### 运行项目

JDK 1.8

Maven

1.mvn package

2.导入sql脚本，修改配置密码

3.启动Application.java

4.浏览器输入 http://localhost:8180

### 项目截图: 

登录页面

![001](https://images.gitee.com/uploads/images/2019/0811/200427_87471f5a_591515.png "001.png")
![002](https://images.gitee.com/uploads/images/2019/0811/200456_1e20ac55_591515.png "002.png")
![003](https://images.gitee.com/uploads/images/2019/0811/200515_e391fb71_591515.png "003.png")
![004](https://images.gitee.com/uploads/images/2019/0811/200526_b90bdceb_591515.png "004.png")
![005](https://images.gitee.com/uploads/images/2019/0811/200539_422ab163_591515.png "005.png")
![006](https://images.gitee.com/uploads/images/2019/0811/200609_c96dcd84_591515.png "006.png")
![007](https://images.gitee.com/uploads/images/2019/0811/200638_bf930ce8_591515.png "007.png")
![008](https://images.gitee.com/uploads/images/2019/0811/200657_d0bf3b8a_591515.png "008.png")



欢迎加群学习架构：   [点此加入QQ群：](https://jq.qq.com/?_wv=1027&k=5xwNtbw) **317945902**  <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=1eddc0d8628b53f65129bb1e44fe342a1aaaa6dc7679739cfbefd7a2e86a15b5"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="SpringBoot SpringCloud Docker " title="SpringBoot SpringCloud Docker "></a>